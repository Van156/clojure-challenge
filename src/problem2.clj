(ns problem2

  (:require [clojure.spec.alpha :as s]
            [clojure.data.json :as json]
            )

  )


(s/def :customer/name string?)
(s/def :customer/email string?)
(s/def :invoice/customer (s/keys :req [:customer/name
                                       :customer/email]))

(s/def :tax/rate double?)
(s/def :tax/category #{:iva})
(s/def ::tax (s/keys :req [:tax/category
                           :tax/rate]))
(s/def :invoice-item/taxes (s/coll-of ::tax :kind vector? :min-count 1))

(s/def :invoice-item/price double?)
(s/def :invoice-item/quantity double?)
(s/def :invoice-item/sku string?)

(s/def ::invoice-item
  (s/keys :req [:invoice-item/price
                :invoice-item/quantity
                :invoice-item/sku
                :invoice-item/taxes]))

(s/def :invoice/issue-date inst?)



(s/def :invoice/items (s/coll-of ::invoice-item :kind vector? :min-count 1))

(s/def ::invoice
  (s/keys :req [:invoice/issue-date
                :invoice/customer
                :invoice/items]))

(def invoice (json/read-json (slurp "invoice.json")))

(use 'clojure.instant)

(defn convert-date [date]
      (let [[day month year] (clojure.string/split date #"/")]
           (read-instant-date (str year "-" month "-" day))))

(defn extract-tax-data [taxes]
      (let [tax (or (first taxes) {})]
           {:tax/rate (double (:tax_rate tax))
            :tax/category (if (= "IVA" (:tax_category tax))
                            :iva
                            :unknown)}))

(defn get-item-data [item]
      {:invoice-item/price (:price item)
       :invoice-item/quantity  (:quantity  item)
       :invoice-item/sku (:sku item)
       :invoice-item/taxes [(extract-tax-data (:taxes item))]})

(defn convert-to-valid-invoice [invoice]
      (let [invoice-data (:invoice invoice)
            issue-date (:issue_date invoice-data )
            customer-data (:customer invoice-data )
            items (:items invoice-data)
            result-map (if-not (and invoice-data customer-data items)
                               {}
                               {:invoice/issue-date (convert-date issue-date)
                                :invoice/customer {:customer/name  (:company_name customer-data)
                                                   :customer/email (:email customer-data)}
                                :invoice/items (into [] (map get-item-data items)) })]
           result-map))





(defn run [opts]
    (println (s/valid? ::invoice  (convert-to-valid-invoice invoice)))
)
