(ns problem1
  (:require [clojure.edn :as edn])
  )
(def invoice (edn/read-string (slurp "invoice.edn")))

(defn is-valid-item [item]
  (let [iva-bool (= 19 (:tax/rate (into {} (:taxable/taxes item))))
        retention-bool (= 1 ( :retention/rate (into {} (:retentionable/retentions item)) ) )
        retention-bool-2 (= 1 ( :retentions/rate (into {} (:retentionable/retentions item)) ) )

        result (cond
                 (and iva-bool (some true? [retention-bool retention-bool-2])) false
                 (or iva-bool retention-bool) true
                 :else false
                 )
        ] result)
  )


(defn remove-duplicate-items-id [items]
  (let [item-ids (frequencies (map :invoice-item/id items))]
    (filter #(= 1 (item-ids (:invoice-item/id %))) items)))


(defn get-valid-items [invoice]
  (->> invoice
       :invoice/items
       (distinct)
       (filter is-valid-item)
       (remove-duplicate-items-id)
       (into [])
       ))

(defn run[opts] 
    (println (get-valid-items invoice))
)