(ns problem3

    )

(defn- discount-factor [{:invoice-item/keys [discount-rate]
                         :as item
                         :or                {discount-rate 0}}]

       (- 1 (/ (or (:discount-rate item) discount-rate) 100.0)))


(defn subtotal
      [ {:invoice-item/keys [precise-quantity precise-price discount-rate]
         :as                item
         :or                {discount-rate 0}  }  ]


      (* (or (:precise-quantity item) 0) (or (:precise-price item) 0) (discount-factor item)   )
      )

(use 'clojure.test)



(deftest test-discount-factor
  (testing "returns 1 when no discount rate is provided"
    (is (= 1.0 (discount-factor {}))))
         (testing "calculates correct discount factor with valid discount rate"
                  (is (= 0.9 (discount-factor {:discount-rate 10})))
                  (is (= 0.8 (discount-factor {:discount-rate 20})))
                  (is (= 0.7 (discount-factor {:discount-rate 30})))
                  (is (= 1.0 (discount-factor {})))
                  ))


(deftest test-subtotal
  (testing "subtotal with no discount"
    (is (= 50.00 (subtotal {:precise-quantity 2
                            :precise-price 25.00}))))
  (testing "subtotal with 50% discount"
    (is (= 25.00 (subtotal {:precise-quantity 2
                            :precise-price 25.00
                            :discount-rate 50}))))
  (testing "subtotal with 25% discount"
    (is (= 56.25 (subtotal {:precise-quantity 3
                            :precise-price 25.00
                            :discount-rate 25}))))
  (testing "subtotal with 0% discount"
    (is (= 100.00 (subtotal {:precise-quantity 4
                             :precise-price 25.00
                             :discount-rate 0.0}))))
  (testing "subtotal with no quantity or price"
    (is (= 0.00 (subtotal {}))))
  (testing "returns 0 when precise-quantity or precise-price is 0"
    (is (= 0.0 (subtotal {:precise-quantity 0
                          :precise-price 10})))
    (is (= 0.0 (subtotal {:precise-quantity 10
                          :precise-price 0}))))
  (testing "calculates correct subtotal with valid data"
    (is (= 200.0 (subtotal {:precise-quantity 10
                            :precise-price 20})))
    (is (= 93.75 (subtotal {:precise-quantity 5
                            :precise-price 25
                            :discount-rate 25})))
    (is (= 25.0 (subtotal {:precise-quantity 2
                           :precise-price 25
                           :discount-rate 50}))))
  )

(defn run[opts] 
    (run-tests 'problem3)
    )

