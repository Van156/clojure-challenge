# Engineering Challenge

## Getting started

This clojure challenge is made up of 3 questions that reflect the learning you accumulated for the past week. Complete the following instructions:

1. Create a Gitlab repo of your own and fork this repo. When complete, send us the link to your challenge results.
1. Duration: 4 hours. Checking your work regularly until the 4 hour mark
1. Create a deps.edn file and include the following dependencies

```
org.clojure/data.json  {:mvn/version "0.2.6"}
```

1. Enjoy!

## Problems

### Problem 1 Thread-last Operator ->>

Given the invoice defined in **invoice.edn** in this GoogleDrive folder, use the thread-last ->> operator to find all invoice items that satisfy the given conditions. Please write a function that receives an invoice as an argument and returns all items that satisfy the conditions described below.

#### Requirements

- Load invoice to play around with the function like this:

```
(def invoice (clojure.edn/read-string (slurp "invoice.edn")))
```

#### Definitions

- An invoice item is a clojure map { … } which has an :invoice-item/id field. EG.

```
{:invoice-item/id     "ii2"
  :invoice-item/sku "SKU 2"}
```

- An invoice has two fields :invoice/id (its identifier) and :invoice/items a vector of invoice items

#### Invoice Item Conditions

- At least have one item that has :iva 19%
- At least one item has retention :ret_fuente 1%
- Every item must satisfy EXACTLY one of the above two conditions. This means that an item cannot have BOTH :iva 19% and retention :ret_fuente 1%.

## Problem 2: Core Generating Functions

Given the invoice defined in **invoice.json** found in this GoogleDrive, generate an invoice that passes the spec **::invoice** defined in **invoice-spec.clj**. Write a function that as an argument receives a file name (a JSON file name in this case) and returns a clojure map such that

```
(s/valid? ::invoice invoice) => true
```

where invoice represents an invoice constructed from the JSON.

## Problem 3: Test Driven Development

Given the function **subtotal** defined in **invoice-item.clj** in this GoogleDrive folder, write at least five tests using clojure core **deftest** that demonstrates its correctness. This subtotal function calculates the subtotal of an invoice-item taking a discount-rate into account. Make sure the tests cover as many edge cases as you can!

## Problem 1

For this point, the given conditions are taken into account, in particular the following was considered from the given file:

1. That in :retentionable/retentions, a map with the keywords :retentions or :retention may be contained.
2. That for each invoice items, items with the same :invoice-item/id but with different data may be present.

## Problem 2

For this point, only the required structure given by the spec was organized.

## Problem 3

For this point, corrections were made to the code by adding in particular the case where if no values are received then the subtotal is 0.
